from abc import ABC, abstractmethod

class IProduct(ABC):
    @abstractmethod
    def get_price(self):
        pass

class Product(IProduct):
    def __init__(self, name, price):
        self._name = name
        self._price = price

    def get_price(self):
        return self._price

class IOrder(ABC):
    @abstractmethod
    def add_product(self, product):
        pass

    @abstractmethod
    def remove_product(self, product):
        pass

    @abstractmethod
    def get_total_amount(self):
        pass

class Order(IOrder):
    def __init__(self):
        self._products = []

    def add_product(self, product):
        self._products.append(product)

    def remove_product(self, product):
        self._products.remove(product)

    def get_total_amount(self):
        total = 0
        for product in self._products:
            total += product.get_price()
        return total

class ICustomer(ABC):
    @abstractmethod
    def place_order(self, order):
        pass

    @abstractmethod
    def get_order_history(self):
        pass

class Customer(ICustomer):
    def __init__(self, name):
        self._name = name
        self._orders = []

    def place_order(self, order):
        self._orders.append(order)

    def get_order_history(self):
        return self._orders

class IShipper(ABC):
    @abstractmethod
    def ship_order(self, order):
        pass

class Shipper(IShipper):
    def __init__(self, name):
        self._name = name

    def ship_order(self, order):
        print(f"{self._name} is shipping order {order}")
def main():
    product1 = Product("Product 1", 100)
    product2 = Product("Product 2", 200)
    product3 = Product("Product 3", 300)

    order = Order()
    order.add_product(product1)
    order.add_product(product2)
    order.add_product(product3)

    assert order.get_total_amount() == 600

    customer = Customer("John Doe")

    ship_order = Shipper("Manoj couriers")

    print("Products:")
    print(product1.get_price())
    print(product2.get_price())
    print(product3.get_price())

    print("Order1:", product1._name, product1._price)
    print("Order2:", product2._name, product2._price)
    print("Order3:", product3._name, product3._price)

    print("Total Amount:", order.get_total_amount())

    print("order", order.add_product("product1"))
    print("The product is Added")

    print("order History is:",customer.get_order_history())

    print("Customer:", customer._name)

    print("ShipOrder:", ship_order._name)

if __name__ == '__main__':
    main()
